<?php
/*
Template Name: Who We Are
*/
get_header(); ?>



<div class="intro">
<div class="bg_image" style="background-image: url('<?php the_field("intro-image"); ?> ');"></div>
 <div class="intro__body">
		<h1 class="intro__title">
			<?php the_field('intro-title'); ?>
		</h1>
		<p class="intro__text">
			<?php the_field('intro-text'); ?>
		</p>
		<div class="intro__values row">
		<?php  if( have_rows('value') ):?>
		    <?php  while ( have_rows('value') ) : the_row(); ?>
			<div class="value large-4 columns">
				<div class="value__icon">
        			<img src="<?php the_sub_field('value-icon');?>" alt="value__icon">
				</div>
				<h3 class="value__title">
					<?php the_sub_field('value-title'); ?>
				</h3>
				<div class="value__text">
					<?php the_sub_field('value-text'); ?>
				</div>
			</div>
				<?php
		    endwhile;
				else :?>
		<?php endif;?>
		</div>
	</div>
</div>

<div class="team">
	<h3 class="team__title">
		Meet The Team
	</h3>
	<div class="team__members row hide-for-small-only"><!-- slider here  -->
	<?php if( have_rows('member') ):
	    while ( have_rows('member') ) : the_row();?>
		<div class="member columns" >
			<div class=" member__image" style="background-image: url('<?php the_sub_field("member_image"); ?> ');"></div>
			<div class="member__tag">
				<p class="member__name"><?php the_sub_field('member_name'); ?></p>
				<p class="member__title"><?php the_sub_field('member_title'); ?></p>
			</div>
			<div class="member__info columns">
				<p class="member__bio"><?php the_sub_field('member_bio'); ?></p>
				<div class="member__channels">
					<a href="<?php the_sub_field('google_link'); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a href="<?php the_sub_field('linkedin_link'); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a href="<?php the_sub_field('twitter_link'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	<?php        
	endwhile;
	else : 
		endif;?>
	</div>
	<div class="team__members--mobile row show-for-small-only"><!-- slider here  -->
	<?php if( have_rows('member') ):
	    while ( have_rows('member') ) : the_row();?>
		<div class="member--mobile columns" >
			<div class=" member--mobile__image" style="background-image: url('<?php the_sub_field("member_image"); ?> ');"></div>
			<div class="member--mobile__tag">
				<p class="member--mobile__name"><?php the_sub_field('member_name'); ?></p>
				<p class="member--mobile__title"><?php the_sub_field('member_title'); ?></p>
			</div>
			<div class="member--mobile__info columns">
				<p class="member--mobile__bio"><?php the_sub_field('member_bio'); ?></p>
				<div class="member--mobile__channels">
					<a href="<?php the_sub_field('google_link'); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					<a href="<?php the_sub_field('linkedin_link'); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a href="<?php the_sub_field('twitter_link'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	<?php        
	endwhile;
	else : 
		endif;?>
	</div>
</div>
<div class="activities show-for-large" id="activities">
	<h3 class="activities__title"> Our Activities</h3>
	<ul class="activities__thumbnail tabs" data-tabs id="activities-tabs" style="background-image: url(<?php echo get_stylesheet_directory_uri();?>/assets/images/whoWeAre/logos/infographidia_2.png)">
	
		<li class="activity service">
			<!-- <a href="#service_design" aria-selected="true"> -->
			<span>
			<?php require_once('svgz/service_design.php'); ?>
			<h6 class="activity__title">Service Design</h6>
			</span>
			<!-- </a> -->
		</li>
		<li class="activity tabs-title architecture">
			<a href="#architecture_design" aria-selected="true">
			<?php require_once('svgz/architecture_design.php'); ?>
				<h6 class="activity__title">Architecture Design</h6>
			</a>
		</li>
		<li class="activity branding">
			<!-- <a href="#branding___visual_communication" aria-selected="true"> -->
			<span>
				<?php require_once('svgz/branding___visual_communication.php'); ?>
				<h6 class="activity__title">Branding & Visual Communication</h6>
			</span>
			<!-- </a> -->
		</li>
		<li class="activity landscape">

			<span>
				<?php require_once('svgz/landscape_design.php'); ?>
				<h6 class="activity__title">Landscape Design</h6>
			</span>

		</li>
		<li class="activity tabs-title strategic">
			<a href="#strategic_innovation" aria-selected="true">
				<?php require_once('svgz/strategic_animation.php'); ?>
				<h6 class="activity__title">Strategic Innovation</h6>
			</a>
		</li>
		<li class="activity tabs-title projectMan">
			<a href="#project___control_managment" aria-selected="true">
				<?php require_once('svgz/project___control_management.php'); ?>
				<h6 class="activity__title">Project & Control Management</h6>
			</a>
		</li>
		<li class="activity product">
			<!-- <a href="#product_design" aria-selected="true"> -->
			<span>
				<?php require_once('svgz/product_design.php'); ?>
				<h6 class="activity__title">Product Design</h6>
			<!-- </a> -->
			</span>
		</li>
		<li class="activity urban">
			<!-- <a href="#urban_design" aria-selected="true"> -->
			<span>
				<?php require_once('svgz/urban_design.php'); ?>
				<h6 class="activity__title">Urban Design</h6>
			<!-- </a> -->
			</span>
		</li>				
	</ul>
	<div class="tabs-content activity__content" data-tabs-content="activities-tabs">

	 <?php 
	      // check if the repeater field has rows of data
if( have_rows('activity_content') ):
$counter=0;

 	// loop through the rows of data
    while ( have_rows('activity_content') ) : the_row();
$title_name= get_sub_field('title');

$title_name = str_replace(" ", "_", $title_name);
$title_name = str_replace("&", "_", $title_name);


// convert the string to all lowercase
$title_name = strtolower($title_name);


        // display a sub field value
?>
  <div class="tabs-panel <?php if($counter==0 ){echo 'is-active' ;} ?>"  id="<?php echo $title_name ?>">
	  <div class="middle">
		  <h3 class="middle__title"><?php  the_sub_field('title'); ?></h3>
		  <p class="middle__text"><?php  the_sub_field('text'); ?></p>
	  </div>
  </div>
        
<?php 
$counter++;

    endwhile;

else :

    // no rows found

endif;

?>
  </div>
  </div>

<section class="activities--mobile section hide-for-large">
    <div class="row regular__row">
      <div class="large-7 large-push-3 columns">
    	<h3 class="activities__title activities--mobile__title"> Our Activities</h3>
        <ul class="accordion activities__list activities--mobile__list" data-accordion>
	        <li class="accordion-item is-active" data-accordion-item>
	            <a href="#" class="accordion-title projectMan--mobile">
		  			<div class="svg_mobile"><?php require('svgz/project___control_management.php'); ?></div>
					<h6 class="activity__title">Project & Control Management</h6>
	               <span><i class="fa fa-angle-up" aria-hidden="true"></i></span>
	              <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
	            </a>
	            <div class="accordion-content projectMan--mobile" data-tab-content>
	              Project & Control Management is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi beatae quaerat a fugit hic aut voluptas voluptatem laboriosam. Distinctio aspernatur vero tempore consectetur iste error! Atque laboriosam vero fuga accusantium.
	            </div>
	        </li>     
        <li class="accordion-item " data-accordion-item>
            <a href="#" class="accordion-title strategic--mobile">
	  			<div class="svg_mobile"><?php require('svgz/strategic_animation.php'); ?></div>
				<h6 class="activity__title">Strategic Innovation</h6>
               <span><i class="fa fa-angle-up" aria-hidden="true"></i></span>
              <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
            </a>
            <div class="accordion-content strategic--mobile" data-tab-content>
				Strategic Innovation is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi beatae quaerat a fugit hic aut voluptas voluptatem laboriosam. Distinctio aspernatur vero tempore consectetur iste error! Atque laboriosam vero fuga accusantium.
				<ul class="sub-activities">
					<li class="sub-activity service--mobile">
						<div class="svg_mobile svg_mobile_sub"><?php require('svgz/service_design.php'); ?></div>
						<h6 class="activity__title activity__title_sub">Service Design</h6>
					</li>
					<li class="sub-activity branding--mobile">
							<div class="svg_mobile svg_mobile_sub"><?php require('svgz/branding___visual_communication.php'); ?></div>
							<h6 class="activity__title activity__title_sub">Branding & Visual Communication</h6>
					</li>
				</ul>
            </div>
        </li>
		<li class="accordion-item " data-accordion-item>
            <a href="#" class="accordion-title architecture--mobile">
	  			<div class="svg_mobile"><?php require('svgz/architecture_design.php'); ?></div>
				<h6 class="activity__title">Architecture Design</h6>
               <span><i class="fa fa-angle-up" aria-hidden="true"></i></span>
              <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>
            </a>
            <div class="accordion-content architecture--mobile" data-tab-content>
				Architecture Design is Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi beatae quaerat a fugit hic aut voluptas voluptatem laboriosam. Distinctio aspernatur vero tempore consectetur iste error! Atque laboriosam vero fuga accusantium.
				<ul class="sub-activities">
					<li class="sub-activity landscape--mobile">
						<div class="svg_mobile svg_mobile_sub"><?php require('svgz/landscape_design.php'); ?></div>
						<h6 class="activity__title activity__title_sub">Landscape Design</h6>
					</li>
					<li class="sub-activity product--mobile">
						<div class="svg_mobile svg_mobile_sub"><?php require('svgz/product_design.php'); ?></div>
						<h6 class="activity__title activity__title_sub">Product Design</h6>
					</li>
					<li class="sub-activity urban--mobile">
						<div class="svg_mobile svg_mobile_sub"><?php require('svgz/urban_design.php'); ?></div>
						<h6 class="activity__title activity__title_sub">Urban Design</h6>
					</li>
				</ul>
            </div>
        </li>         
       </ul>
      </div>  
    </div>
</section>

<?php get_footer(); ?>