<?php
/*
*/
get_header(); ?>
<!-- <div class="wrapper">
  <div class="process" id="processContainer">
    <div class="process__banner" id="slideContainer" >
       <div class="panel one">
          <h1 class="">Ever since I<br>left the city you</h1> 
        </div>
        <div class="panel two">
          <h1>Got a reputation<br>for yourself now</h1>
        </div>
        <div class="panel three">
          <h1>three</h1>
        </div>
        <div class="panel four">
          <h1>four</h1>
        </div> 
        <div class="panel five">
          <h1>five</h1>
        </div>    
    </div>
  </div>
</div>
<div class="nothing">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus ut ratione ipsum doloribus sint fugiat, aut suscipit ipsam voluptas eligendi, voluptates odio obcaecati doloremque beatae ullam reiciendis. Voluptas, placeat, explicabo!</div>
<div class="nothing">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus ut ratione ipsum doloribus sint fugiat, aut suscipit ipsam voluptas eligendi, voluptates odio obcaecati doloremque beatae ullam reiciendis. Voluptas, placeat, explicabo!</div>
<div class="nothing">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus ut ratione ipsum doloribus sint fugiat, aut suscipit ipsam voluptas eligendi, voluptates odio obcaecati doloremque beatae ullam reiciendis. Voluptas, placeat, explicabo!</div>
<div class="nothing">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus ut ratione ipsum doloribus sint fugiat, aut suscipit ipsam voluptas eligendi, voluptates odio obcaecati doloremque beatae ullam reiciendis. Voluptas, placeat, explicabo!</div>
 -->
<!--  <div id="fullpage" class="fullpage-wrapper">
  <div class="section fp-section fp-table active fp-completely" id="section0" data-anchor="start" style="height: 440px;">
    <div class="fp-tableCell" style="height: 440px;">
      <div class="intro">
        <div class="block-1-content">
          <div class="title">WINTER CAPITAL</div>
            <p>an investment advisory firm <br> focused on private equity and <br> venture capital in Russia and Europe
            </p>
        </div>
      </div>
    </div>
  </div>
  <div class="section fp-section fp-table" id="section1" data-anchor="keyfacts" style="height: 440px;">
    <div class="fp-tableCell" style="height: 440px;">
      <div class="intro">
        <div class="ramka"></div>
        <div class="block-2-content">
          <div class="title">KEY FACTS</div>
          <ul>
            <li class="ico1">Team of <a href="/team">10 professionals</a></li>
            <li class="ico2">Over <a href="/portfolio">$300mn invested</a> by Winter Capital clients</li>
            <li class="ico3"><a href="/portfolio">7 portfolio</a> companies</li>
          </ul>            
        </div>
      </div>
    </div>
  </div>
  <div id="section2" class="section fp-section fp-table" data-anchor="investment-approach" style="height: 440px;">
    <div class="fp-tableCell" style="height: 440px;">
      <div class="intro">
        <div class="block-3-content">
          <div class="title">APPROACH</div>
          <ul>
            <li class="num1">Genuine partners, not merely financial investors</li>
            <li class="num2">Open to any interesting investment idea</li>
            <li class="num3">Long-term capital providers</li>
          </ul>            
        </div>
      </div>
    </div>
  </div>
</div> -->
<div class="big">
<div class="wrapper" id="windo">
  <div class="wrapper__slider" id="viewer" style="background-image: url('<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/panorama.jpg');">
    <!-- <img src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/process__banner.png" alt="process__banner"> -->
  </div>
  <div class="wrapper__overlay" >
    <section class="wrapper__logo panel logo--shine" id="logo__0">
      <img class="logo__circle logo--rotate" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/circle1.png" alt="">
      <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="">
      <p class="logo__caption">Insight</p>
    </section>
    <section class="wrapper__logo panel" id="logo__1">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/circle1.png" alt="">
      <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="">
      <p class="logo__caption">Define</p>
    </section>
    <section class="wrapper__logo panel" id="logo__2">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/circle1.png" alt="">
      <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="">
      <p class="logo__caption">Ideate</p>
    </section>
    <section class="wrapper__logo panel" id="logo__3">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/circle1.png" alt="">
      <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="">
      <p class="logo__caption">Action</p>
    </section>
    <section class="wrapper__logo panel" id="logo__4">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/circle1.png" alt="">
      <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="">
      <p class="logo__caption">Scale</p>
    </section>    
  </div>

  <button class="button button__prev">Previous</button>
  <button class="button button__next">Next</button>

</div>
<div class="">
  <?php
    $args = array(
      'post_type' => 'design',
      'posts_per_page'=>3
    );
    $designs = new WP_Query( $args );
    if( $designs->have_posts() ) {?>
        <div class="row postsContainer" data-equalizer>
          <h2 class="postsContainer__title">
            More Articles
          </h2>
      <?php  while( $designs->have_posts() ) {
          $designs->the_post();
          ?>
          <div class="large-6 columns morePost" data-equalizer-watch>
            <div class="large-5 columns morePost__image" >
              <?php  the_post_thumbnail( 'large' );?>
            </div>
            <div class="large-7 columns morePost__text">
          <h5 class="morePost__title"><?php the_title() ?></h5>
          <div class="content morePost__excerpt">
            <?php the_excerpt(); ?>
          </div>
            </div>
          </div>
        <?php
      }
    } ?>
      </div>
  <div class="verge">
    <a href="#" class="button button__blue">
      Load More Articles 
    </a>  
  </div>
</div>

</div>
<!-- here a page-timeline  -->
 <?php


get_header(); ?>

<div class="timeline">
  <ul class="tabs timeline__tabs" data-tabs id="example-tabs">
    <li class="tabs-title timeline__tab is-active"><a href="#panel1" >News</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel2">1995 - 2000</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel3">2001 - 2005</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel4">2006 - 2010</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel5">2011 - 2015</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel6">2016 - 2020</a></li>
    <li class="tabs-title timeline__tab"><a href="#panel7">All Projects</a></li>
  </ul>
  <div class="tabs-content timeline__panels" data-tabs-content="example-tabs">
    <div class="tabs-panel timeline__panel is-active" id="panel1">
    <h1>TEXT1</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
    <div class="row medium-up-4 gallery">
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n1.jpg');"></div>
        <div class="caption">
          <div class="caption__dimmed"></div>
          <h4 class="caption__title">Latest News</h4>
          <p class="caption__body ">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna .</p>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
        </div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n2.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n3.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n4.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n4.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n3.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n2.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n1.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n1.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n2.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n3.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n4.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n4.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n3.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n2.jpg');"></div>
      </div>
      <div class="gallery__item columns">
        <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/n4.jpg');"></div>
      </div>
    </div>
    </div>
    <div class="tabs-panel timeline__panel" id="panel2">
      <h1>TEXT2</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
      <div class="tabs-panel timeline__panel" id="panel3">
    <h1>TEXT3</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
      <div class="tabs-panel timeline__panel" id="panel4">
    <h1>TEXT4</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
      <div class="tabs-panel timeline__panel" id="panel5">
    <h1>TEXT5</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
      <div class="tabs-panel timeline__panel" id="panel6">
      <h1>TEXT6</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
      <div class="tabs-panel timeline__panel" id="panel7">
    <h1>TEXT7</h1>
      <p>Suspendisse dictum feugiat nisl ut dapibus.  Vivamus hendrerit arcu sed erat molestie vehicula. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor.  Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor.</p>
    </div>
  </div>


</div>

<?php get_footer();











<script type="text/javascript">
    $(document).ready(function(){
        var $offset = parseInt($("[data-load-posts]").attr('data-load-posts'));
        var exclude = $("[data-load-posts]").attr('data-exclude');
        $("[data-load-posts]").click(function(e){
            e.preventDefault();
            $.ajax({
                url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type:"GET",
                data:{action:'get_posts',offset:$offset,exclude:exclude}
                })
                .done(function(response){
                    var $response = JSON.parse(response);
                    // if ($response.length == 0 ) {
                    //     $(".verge").remove();
                    // }
                    console.log(response)
                    for (var i = 0; i <= $response.posts.length-1; i++){
                        $(".postsContainer").append(`
                          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'small' );?>
              <div class="gallery__item columns">
              <?php if ($thumb['0']) {?>
              <div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div>
      <?php }else{ ?>
      <div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div>
      <?php } ?>
        <div class="caption">
          <div class="caption__dimmed"></div>
          <h5 class="caption__title">${$response.posts[i]['title']}</h5>
          <div class="caption__body ">${$response.posts[i]['excerpt']}</div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
        </div>
      </div>`)
                
                    };

                    if($response.next_posts == 0) {
                         $(".verge").remove();
                    }

                    $offset+= 2;
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function() {
                    console.log( "complete");
                });
        });
    });
    </script>


<?php get_footer();

