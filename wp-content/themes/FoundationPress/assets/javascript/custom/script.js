$(function () {
    $('.topSlider').slick({
        prevArrow: "<button type='button' class='slick-prev'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        nextArrow: "<button type='button' class='slick-next'><i class='fa fa-angle-right' aria-hidden='true'></i></button>"
    });
    // Scroll Down Arrows in the Homepage 
    var offset = $('.header').outerHeight();
    $(window).resize(function () {
        offset = $('.header').outerHeight();
    });
    $('a[href^="#section0"]').on('click', function (event) {

        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            $('html, body').stop().animate({
                scrollTop: target.offset().top - offset
            }, 1000);
        }
    });
    var here = $('#activities');
    $('.container').css("margin-top", offset+"px");
    // Contact Us Page Hover Effect 
    $('a.office__info').hover(function () {
        var parent = $(this).parents('.office');
        parent.find('.office__front').toggleClass('hidden');
        parent.find('.office__title').toggleClass('office__title--back');
        parent.find('.office__address').toggleClass('hidden');
        parent.find('.office__email').toggleClass('hidden');
    });

    // Contact Form Animation
    $('#field_1_6').append('<span class="placeholder">Name</span>');
    $('#field_1_4').append('<span class="placeholder">Email</span>');
    $('#field_1_5').append('<span class="placeholder">Send us a Message</span>');
    $('#field_1_6').append('<span class="placeholder--mobile hide-for-large">Name</span>');
    $('#field_1_4').append('<span class="placeholder--mobile hide-for-large">Email</span>');
    $('#field_1_5').append('<span class="placeholder--mobile hide-for-large">Send us a Message</span>');


    $('.gfield input').focus(function () {
        var field = $(this).parents("li");
        field.find('.gfield_label').toggleClass('move');
        field.find('.placeholder').addClass('placeholder--movedown');
        field.find('.placeholder--mobile').addClass('hidden');
    });
    $('.gfield input').blur(function () {
        var field = $(this).parents("li");
        field.find('.gfield_label').toggleClass('move');
        if ($(this).val().length === 0) {
            field.find('.placeholder').removeClass('placeholder--movedown');
            field.find('.placeholder--mobile').removeClass('hidden');
        } else {
            field.find('.placeholder').addClass('placeholder--movedown');
            field.find('.placeholder--mobile').addClass('hidden');
        }
    });

    $('.gfield textarea').click(function () {
        console.log("textarea is true");
        var field = $(this).parents("li");
        field.find('.gfield_label').toggleClass('move--box');
        field.find('.placeholder').addClass('placeholder--movedown_textarea');
        field.find('.placeholder--mobile').addClass('hidden');
    });
    $('.gfield textarea').blur(function () {
        console.log("textarea is false");
        var field = $(this).parents("li");
        field.find('.gfield_label').toggleClass('move--box');
        if ($(this).val().length === 0) {
            field.find('.placeholder').removeClass('placeholder--movedown_textarea');
            field.find('.placeholder--mobile').removeClass('hidden');
        } else {
            field.find('.placeholder').addClass('placeholder--movedown_textarea');
            field.find('.placeholder--mobile').addClass('hidden');
        }
    });
});