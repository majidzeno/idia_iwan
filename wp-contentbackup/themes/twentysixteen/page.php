<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="landing">
      
      <div class="header">
        <div class="row">
          <div class="small-12 medium-6 text-center medium-text-left columns">
            <img id="logo" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="">
          </div>
          <!-- <div class="small-12 medium-6 text-center medium-text-right columns">
            <img id="text" src="<?php echo get_template_directory_uri(); ?>/img/text.png" alt="">
          </div> -->
        </div>
      </div>

      <div class="visual">
        <img src="<?php echo get_template_directory_uri(); ?>/img/visual.jpg" alt="">
      </div>

      <div class="footer">
        <div class="row">
          <div class="small-12 medium-4 text-center medium-text-left columns">
            <div class="address">
            CAI.: 45, Michael Bakhoum St., Dokki, Giza, Egypt - 12311 <br>
            DXB.: Design District, Premises 106, Building 8, Dubai
            </div>
          </div>

          <div class="small-12 medium-4 medium-push-4 text-center columns">
            <div class="email text-center medium-text-left">
              Contact Us: <br>
              <a href="mailto:info@iwandesigns.com">info@iwandesigns.com</a>
            </div>
          </div>


          <div class="small-12 medium-4 medium-pull-4 text-center columns">
            <div class="cs">
            Coming Soon
            </div>
            <div class="credit">
            Copyright © IWAN DESIGNS. 2016
            </div>
          </div>
          
        </div>
      </div>

    </div>

<?php get_footer(); ?>
