 <?php
/*
Template Name: Timeline
*/
get_header(); ?>

<div class="timeline">
	<ul class="tabs timeline__tabs show-for-large" data-tabs id="example-tabs">
	  <li class="tabs-title timeline__tab is-active"><a href="#panel1" id="tab_1" >News</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel6" id="tab_6">2016 - 2020</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel5" id="tab_5">2011 - 2015</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel4" id="tab_4">2006 - 2010</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel3" id="tab_3">2001 - 2005</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel2" id="tab_2">1995 - 2000</a></li>
	  <li class="tabs-title timeline__tab"><a href="#panel7" id="tab_7">All Projects</a></li>
	</ul>
	<ul class="hide-for-large dropdown timeline__menu" data-dropdown-menu>
	    <li>
	    	<a href="#">Menu
				<ul class="tabs timeline__items" data-tabs id="example-tabs">
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_1">News</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_6">2016 - 2020</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_5">2011 - 2015</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_4">2006 - 2010</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_3">2001 - 2005</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_2">1995 - 2000</a></li>
					<li class="timeline__item tabs-title"><a href="#" class="tPanel" data-target="#tab_7">All Projects</a></li>
				</ul>
		    </a>
	    </li>
	</ul>
	<div class="tabs-content timeline__panels" data-tabs-content="example-tabs">
	<!-- ....................................................NEWS Section................................................... -->
	  <div class="tabs-panel timeline__panel is-active" id="panel1">
	  <h1 class="timeline__title"><?php the_field('news_title'); ?></h1>
	    <p><?php the_field('news_intro'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'news',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),

    );
    $news = new WP_Query( $args );
    $posts = $news->found_posts;
    if( $news->have_posts() ) {
     while( $news->have_posts() ) {
            $news->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<div class="caption">
					<div class="caption__dimmed show-for-large"></div>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<div class="caption">
			<?php } ?>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
	<!-- .................................................... 1995 - 2000 ................................................... -->
  <div class="tabs-panel timeline__panel" id="panel2">

	  <h1 class="timeline__title"><?php the_field('projects_title_1995_to_2000'); ?></h1>
	    <p><?php the_field('projects_intro_1995_to_2000'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),
		'tax_query' => array(
		array(
			'taxonomy' => 'project_period',
			'field'    => 'slug',
			'terms'    => 'period_1',
		),
	),
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... 2001 - 2005 ................................................... -->
    <div class="tabs-panel timeline__panel" id="panel3">
	  <h1 class="timeline__title"><?php the_field('projects_title_2001_to_2005'); ?></h1>
	    <p><?php the_field('projects_intro_2001_to_2005'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),
		'tax_query' => array(
		array(
			'taxonomy' => 'project_period',
			'field'    => 'slug',
			'terms'    => 'period_2',
		),
	),
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... 2006 - 2010 ................................................... -->
	    <div class="tabs-panel timeline__panel" id="panel4">
	  <h1 class="timeline__title"><?php the_field('projects_title_2006_to_2010'); ?></h1>
	    <p><?php the_field('projects_intro_2006_to_2010'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),
		'tax_query' => array(
		array(
			'taxonomy' => 'project_period',
			'field'    => 'slug',
			'terms'    => 'period_3',
		),
	),
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... 2011 - 2015 ................................................... -->
    <div class="tabs-panel timeline__panel" id="panel5">
	  <h1 class="timeline__title"><?php the_field('projects_title_2011_to_2015'); ?></h1>
	    <p><?php the_field('projects_intro_2011_to_2015'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),
		'tax_query' => array(
		array(
			'taxonomy' => 'project_period',
			'field'    => 'slug',
			'terms'    => 'period_4',
		),
	),
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... 2016 - 2020 ................................................... -->
	    <div class="tabs-panel timeline__panel" id="panel6">
	  <h1 class="timeline__title"><?php the_field('projects_title_2016_to_2020'); ?></h1>
	    <p><?php the_field('projects_intro_2016_to_2020'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID),
		'tax_query' => array(
		array(
			'taxonomy' => 'project_period',
			'field'    => 'slug',
			'terms'    => 'period_5',
		),
	),
    );
  
    
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... ALL Projects ................................................... -->
    <div class="tabs-panel timeline__panel" id="panel7">
	  <h1 class="timeline__title"><?php the_field('all_projects_title'); ?></h1>
	    <p><?php the_field('projects_intro_all_projects'); ?></p>
		<div class="row medium-up-4 gallery">
		<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>16,
      'post__not_in'=>array($post->ID)
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {
     while( $projects->have_posts() ) {
            $projects->the_post();?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );?>
			<div class="gallery__item columns">
			<?php if ($thumb['0']) {?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo $thumb['0'];?>')"></div></a>
			<?php }else{ ?>
			<a href="<?php echo get_permalink($post->ID) ?>"><div class="gallery__image" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/Post/default.jpg')"></div></a>
			<?php } ?>
				<div class="caption">
					<div class="caption__dimmed"></div>
					<h5 class="caption__title"><?php the_title() ?></h5>
					<div class="caption__body "><?php custom_length_excerpt(20); ?></div>
                    <a class="caption__more" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
				</div>
			</div>
		<?php  }
		}
		wp_reset_postdata();
		?>	
			</div>	
		</div>	
<!-- .................................................... Partners ................................................... -->

<div class="partners">
	<h2 class="partners__title">Our Partners</h2>
	<div class="partners__slider row">
	<?php if( have_rows('partners') ):
    while ( have_rows('partners') ) : the_row();?>

        <div><div style="background-image: url('<?php the_sub_field("partner");?>'" class="partners__slide large-3 columns"></div></div>

    <?php endwhile;
else :

endif;

?>
  </div>
</div>
<script>
$( window ).load(function() {
	<?php if( isset($_GET['tab'])) { ?>
	$('#<?php echo $_GET['tab'] ?>').trigger('click');
	<?php } ?>
});
</script>
<?php get_footer() ;