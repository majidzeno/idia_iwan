<?php
/*
Template Name: contactUs
*/
get_header(); ?>

<?php

// check if the repeater field has rows of data
if( have_rows('offices') ):

  // loop through the rows of data
    while ( have_rows('offices') ) : the_row();?>
<div class="office row show-for-large">
    <div class="office__back" style="background-image: url(<?php the_sub_field('back-image') ;?>)">
      <div class="office__front" style="background-image: url(<?php the_sub_field('front-image') ;?>)">
    </div>
    <a class="office__info" href="<?php the_sub_field('link')?>" target="_blank">
        <span class="office__title"><?php the_sub_field('front-text') ?></span>
        <span class="office__address hidden"><?php the_sub_field('office-address') ?></span>
        <span class="office__email hidden"><?php the_sub_field('email') ?></span>
    </a>
  </div>
</div>
<div class="office--mobile hide-for-large">
    <div class="office--mobile__front" style="background-image: url(<?php the_sub_field('front-image') ;?>)">
      <a class="office--mobile__container" href="<?php the_sub_field('link')?>" target="_blank">
          <div class="office--mobile__title"><?php the_sub_field('front-text') ?></div>
          <div class="office--mobile__info ">
            <div class="office--mobile__address"><?php the_sub_field('office-address') ?></div>
            <div class="office--mobile__email"><?php the_sub_field('email') ?></div>
          </div>
      </a>
  </div>
</div>
    <?php endwhile;
      else :
    ?>
<?php endif;?>
  <div class="form">
    <h2 class="form__title">Ask Us For Help</h2>
    <?php echo do_shortcode('[gravityform id="1" title="false" description="true" ajax="true"]') ?>
  </div>
<?php get_footer();





