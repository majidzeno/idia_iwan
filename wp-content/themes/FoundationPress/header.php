<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php do_action( 'foundationpress_after_body' ); ?>

		<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
			
		<div class="off-canvas-wrapper">
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
			<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
		<?php endif; ?>

		<?php do_action( 'foundationpress_layout_start' ); ?>
		<div class="header">
			<div class="row">
				<a class="show-for-large" href="<?php the_permalink('4');?>"><img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="logo"></a>
				<div class="header__menu show-for-large">
					<?php main_menu(); ?>
					<div class="page_title"><?php echo get_the_title(); ?></div>
					<div class="nav-icon1">
					  <span></span>
					  <span></span>
					  <span></span>
					</div>				
				</div>
				<div class="header__menu_mobile hide-for-large">
					<div class="header__top">
						<div class="nav-icon1">
						  <span></span>
						  <span></span>
						  <span></span>
						</div>				
						<a class="logo_link"href="<?php the_permalink('4');;?>"><img id="logo_mobile" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png" alt="logo_mobile"></a>
					</div>
					<?php main_menu_mobile(); ?>
				</div>
			</div>
		</div>
		<section class="container">
			<?php do_action( 'foundationpress_after_header' );
