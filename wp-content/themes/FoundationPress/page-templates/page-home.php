<?php
/*
Template Name: HOME
*/
get_header(); ?>

  <?php
    // check if the repeater field has rows of data
    if( have_rows('slider') ): ?>
    <div class="topSlider show-for-large">
      <?php // loop through the rows of data
        while ( have_rows('slider') ) : the_row();?>
            <div class="topSlider__image" style="background-image: url(<?php the_sub_field('image') ;?> )">
              <div class="scroll_down">
                <a href="#section0">
                  <span class="arrow_down__left">&#8595;</span>           
                  <span>Find Out More </span>
                  <span class="arrow_down__right">&#8595;</span> 
                </a>     
              </div>
            </div>
        <?php endwhile; ?>
    </div>
    <div class="topSlider hide-for-large"><!-- mobile view-->
      <?php // loop through the rows of data
        while ( have_rows('slider') ) : the_row();?>
            <div class="topSlider__image" style="background-image: url(<?php the_sub_field('image') ;?> )">
                <a href="#section0">
                </a>     
            </div>
        <?php endwhile; ?>
    </div>
    <?php else :
    endif;?>
    <?php
if( have_rows('sections') ):
$sections = get_field('sections');
for($i = 0; $i < count($sections); $i++){
  $section = $sections[$i];
?>
<div class="section" id="section<?php echo $i ?>" style="background-image: url(<?php echo $section['image'] ;?> )" data-type="background" data-speed="10">
  <div class="section__text">
    <div class="section__title"><?php  echo $section['title'];?></div>
    <div class="section__subtitle"><?php echo $section['subtitle']; ?></div>
    <?php if($i < count($sections)){ ?>
   <div class="scroll_down scroll_down--sections">
    <a href="<?php echo $section['link']; ?>">
      <span class="show-for-large">Find Out More </span>
    </a>  
    </div>
    <?php } ?>
  </div>
</div>
 <?php } ?>
<?php  else :

endif;?>

<?php get_footer();

