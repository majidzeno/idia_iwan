<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>		</section>
			<footer>
				<div class="row footer">
					<div class="large-4  columns footer__section show-for-large">
						<h4>Menu</h4>
						<div class="footer__content">
						<?php footer_menu(); ?>
						</div>
					</div>
					<div class="large-4 columns footer__section footer__mobile hide-for-large">
						<h4>Menu</h4>
						<div class="footer__content">
						<?php footer_menu_mobile(); ?>
					</div>
					</div>
					<div class="large-2 large-push-1 columns footer__section footer__mobile">
						<h4>Cairo</h4>
						<div class="footer__content">
							<p>45,Michael Bakhoum St.Dokki,Giza, Egypt - 12311</p>
							<p class="email">cai@idia.design</p>
						</div>
					</div>
					<div class="large-2 large-push-2 columns footer__section footer__mobile">
						<h4>Dubai</h4>
						<div class="footer__content">
							<p>D3, Premises106, Building8, Dubai, UAE.</p>
							<p class="email">dxb@idia.design</p>
						</div>
					</div>
					<div class="large-3 large-push-1 columns footer__section footer__mobile">
						<h4>Join the team</h4>
						<div class="footer__content">
						Send Your CV at lorem@iwandesigns.com
							<div class="social">
								<a href="<?php the_field('pinterest_link');?>" target="_blank"><i class="fa fa-pinterest-square" aria-hidden="true"></i></a>
								<a href="<?php the_field('facebook_link');?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="<?php the_field('linkedin_link');?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
								<a href="<?php the_field('instagram_link');?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							</div>
							<p class="trademark">@ 2017 IDIA</p>
						</div>
					</div>
				</div>	
			</footer>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
