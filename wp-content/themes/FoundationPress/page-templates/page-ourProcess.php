<?php
/*
Template Name: Our Process
*/
get_header(); ?>
<div class="big">
<div class="panorama show-for-large" id="windo" style="background-image: url('<?php the_field("panorama") ?>');">
  <div class="panorama__overlay">
  <h1 class="panorama__title">Our Design Thinking Process</h1>
    <section class="panorama__logo panel logo--shine">
    <div class="sec">
      <img class="logo__circle logo--rotate" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/insight.png" alt="icon">
      <img class="logo__item" src="<?php the_field('insight_logo') ?>" alt="icon">
      <p class="logo__caption">Insight</p>
      </div>
    </section>
    <section class="panorama__logo panel">
    <div class="sec">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Define.png" alt="icon">
      <img class="logo__item" src="<?php the_field('define_logo') ?>" alt="icon">
      <p class="logo__caption">Define</p>
      </div>
    </section>
    <section class="panorama__logo panel">
    <div class="sec">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/ideate.png" alt="icon">
      <img class="logo__item" src="<?php the_field('ideate_logo') ?>" alt="icon">
      <p class="logo__caption">Ideate</p>
    </div>
    </section>
    <section class="panorama__logo panel">
    <div class="sec">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Action.png" alt="icon">
      <img class="logo__item" src="<?php the_field('action_logo') ?>" alt="icon">
      <p class="logo__caption">Action</p>
    </div>
    </section>
    <section class="panorama__logo panel">
    <div class="sec">
      <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Scale.png" alt="icon">
      <img class="logo__item" src="<?php the_field('scale_logo') ?>"  alt="icon">
      <p class="logo__caption">Scale</p>
      </div>
    </section> 
    <div class="panorama__text">
    <div class="text__insight"><?php echo get_field('insight')?></div>
    <div class="text__define"><?php echo get_field('define')?></div>
    <div class="text__ideate"><?php echo get_field('ideate')?></div>
    <div class="text__action"><?php echo get_field('action')?></div>
    <div class="text__scale"><?php echo get_field('scale')?></div>
    </div>  
  </div>
</div>
<div class="panorama--mobile  hide-for-large" style="background-image: url('<?php the_field("panorama") ?>');">
  <div class="panorama__overlay">
    <h1 class="panorama__title">Our Design Thinking Process</h1>
    <div class="panorama__slider">
      <div class="panorama__slide">
        <div class="logo--mobile">
          <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/insight.png" alt="icon">
          <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/lense.png" alt="icon">
          <p class="logo__caption">Insight</p>
        </div>
        <div class="panorama__text text__insight"><?php echo get_field('insight')?></div>
      </div>
      <div class="panorama__slide"> 
        <div class="logo--mobile">
          <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Define.png" alt="icon">
          <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/lamba.png" alt="icon">
          <p class="logo__caption">Define</p>
        </div>
        <div class="panorama__text text__define"><?php echo get_field('define')?></div>
      </div>
      <div class="panorama__slide">
        <div class="logo--mobile">
          <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/ideate.png" alt="icon">
          <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/define-icon.png" alt="icon">
          <p class="logo__caption">Ideate</p>
        </div>
      <div class="panorama__text text__ideate"><?php echo get_field('ideate')?></div>
      </div>
      <div class="panorama__slide">
        <div class="logo--mobile">
          <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Action.png" alt="icon">
          <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Brain.png" alt="icon">
          <p class="logo__caption">Action</p>
        </div>
        <div class="panorama__text text__action"><?php echo get_field('action')?></div>
      </div>
      <div class="panorama__slide">
        <div class="logo--mobile">
          <img class="logo__circle" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Scale.png" alt="icon">
          <img class="logo__item" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/ourProcess/Launch.png" alt="icon">
          <p class="logo__caption">Scale</p>
        </div>
        <div class="panorama__text text__scale"><?php echo get_field('scale')?></div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="posts">
  <?php
    $args = array(
      // 'post_type' => array('article','news'),
      'post_type' => array('article'),
      'posts_per_page'=>3,
      'post__not_in'=>array($post->ID)
    );
    $count=0;
    $cPosts = new WP_Query( $args );
    if( $cPosts->have_posts() ) {?>
        <div class="row postsContainer" data-equalizer>

      <?php  while( $cPosts->have_posts() ) {
        $count++;
        if ($count ==1) {
          $cPosts->the_post();
          $posts=$cPosts->found_posts;
          ?>
          <!-- // Full Page MorePost  -->
          <div class="large-12 full_post columns morePost">
            <div class="row" data-equalizer>
              <div class="large-6 small-12 columns full_post morePost__image" data-equalizer-watch>
                <?php  the_post_thumbnail( 'large' );?>
              </div>
              <div class="large-6 small-12 columns morePost__text" data-equalizer-watch>
                <a href="<?php the_permalink() ?>"><h5 class="morePost__title"><?php the_title() ?></h5></a>
                <div class="content morePost__excerpt">
                     <?php the_excerpt(); ?>
                    <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                </div>
              </div>
            </div>
          </div>
        <?php 
          } 
          $cPosts->the_post();
          ?>
<!-- Half Page MorePost -->
          <div class="large-6 small-12 columns morePost" data-equalizer-watch>
            <div class="" data-equalizer>
              <div class="large-5 small-12 columns morePost__image" data-equalizer-watch>
                <?php  the_post_thumbnail( 'large' );?>
              </div>
              <div class="large-7 small-12 columns morePost__text" data-equalizer-watch>
                <a href="<?php the_permalink();?>"><h5 class="morePost__title"><?php the_title() ?></h5></a>
                <div class="content morePost__excerpt">
                  <?php the_excerpt(); ?>
                  <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                </div>
              </div>
            </div>
          </div>
        <?php
      }
    } ?>
      </div>
  <?php if ($posts > 3) {?>
  <div class="verge">
        <a href="#" class="button button__blue" data-load-posts="2" data-exclude = "<?php echo $post->ID ?>">
            Load More Articles 
        </a>    
    </div>
  <?php } ?>

</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var $offset = parseInt($("[data-load-posts]").attr('data-load-posts'));
        var exclude = $("[data-load-posts]").attr('data-exclude');
        $("[data-load-posts]").click(function(e){
            e.preventDefault();
            $.ajax({
                url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type:"GET",
                data:{action:'get_posts',offset:$offset,exclude:exclude}
                })
                .done(function(response){
                    var $response = JSON.parse(response);
                    // if ($response.length == 0 ) {
                    //     $(".verge").remove();
                    // }
                    console.log(response)
                    for (var i = 0; i <= $response.posts.length-1; i++){
                        $(".postsContainer").append(`
                          <div class='large-6 columns morePost' data-equalizer-watch>
                            <div data-equalizer>
                              <div class='large-5 small-12 columns morePost__image' data-equalizer-watch>${$response.posts[i]['thumbnail']}</div>
                              <div class='large-7 small-12 columns morePost__text' data-equalizer-watch>
                                <a href="${$response.posts[i]['link']}">
                                  <h5 class='morePost__title'>${$response.posts[i]['title']}</h5>
                                </a>
                                <div class='content morePost__excerpt'>${$response.posts[i]['excerpt']}</div>
                                <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                              </div>
                            </div>
                          </div>`);
                          Foundation.reInit($('[data-equalizer]'))
                    };

                    if($response.next_posts == 0) {
                         $(".verge").remove();
                    }

                    $offset+= 2;
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function() {
                    console.log( "complete");
                });
        });
    });
    </script>
<?php get_footer();

