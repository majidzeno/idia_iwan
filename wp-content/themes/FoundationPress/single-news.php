<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
get_header(); ?>

<div id="single-post" role="main">

<?php while ( have_posts() ) : the_post(); ?>
    <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
        <header>
          <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
            <div class="bg_image" style="background-image: url('<?php echo $image[0] ?>');">
                <div class="dimmed" style="background-image: url('<?php echo get_stylesheet_directory_uri()?>/assets/images/Post/dimmed.png');">
                </div>
                <div class="pattern" style="background-image: url('<?php echo get_stylesheet_directory_uri()?>/assets/images/Post/Pattern.png');"></div>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            </div>
        </header>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
    </article>
<?php endwhile;?>
</div>
<!--Custom MorePosts  -->
<?php
    $args = array(
      'post_type' => 'news',
      'posts_per_page'=>2,
      'post__not_in'=>array($post->ID)
    );
    $news = new WP_Query( $args );
    $posts = $news->found_posts;
    if( $news->have_posts() ) {?>
        <div class="row postsContainer" data-equalizer>
            <h2 class="postsContainer__title">
                More Articles
            </h2>
        <?php  while( $news->have_posts() ) {
            $news->the_post();
            ?>
            <div class="large-6 small-12 columns morePost" data-mh="post">
                <div class="" >
                    <div class="large-5 small-12 columns morePost__image" data-mh="post_content">
                        <?php  the_post_thumbnail( 'large' );?>
                    </div>
                    <div class="large-7 small-12 columns morePost__text" data-mh="post_content">
                        <a href="<?php the_permalink(); ?>"><h5 class="morePost__title"><?php the_title() ?></h5></a>
                        <div class="content morePost__excerpt">
                            <?php the_excerpt(); ?>
                            <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
      }
    } ?>
        </div>
        <!-- Load More Button  -->
    <?php if($posts > 2) { ?>
    <div class="verge">
        <a href="#" class="button button__blue" data-load-posts="2" data-exclude = "<?php echo $post->ID ?>">
            Load More News 
        </a>    
    </div>
    <?php } ?>


<script type="text/javascript">
    $(document).ready(function(){
        var $offset = parseInt($("[data-load-posts]").attr('data-load-posts'));
        var exclude = $("[data-load-posts]").attr('data-exclude');
        $("[data-load-posts]").click(function(e){
            e.preventDefault();
            $.ajax({
                url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type:"GET",
                data:{action:'get_news',offset:$offset,exclude:exclude}
                })
                .done(function(response){
                    var $response = JSON.parse(response);
                    // if ($response.length == 0 ) {
                    //     $(".verge").remove();
                    // }
                    console.log(response)
                    for (var i = 0; i <= $response.posts.length-1; i++){
                             $(".postsContainer").append(`
                          <div class='large-6 columns morePost' data-mh="post">
                            <div >
                              <div class='large-5 small-12 columns morePost__image' data-mh="post_content">${$response.posts[i]['thumbnail']}</div>
                              <div class='large-7 small-12 columns morePost__text' data-mh="post_content">
                                <a href="${$response.posts[i]['link']}">
                                  <h5 class='morePost__title'>${$response.posts[i]['title']}</h5>
                                </a>
                                <div class='content morePost__excerpt'>${$response.posts[i]['excerpt']}</div>
                                <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                              </div>
                            </div>
                          </div>`);
                    };
                    jQuery.fn.matchHeight._apply('[data-mh]');
                    
                    if($response.next_posts == 0) {
                         $(".verge").remove();
                    }

                    $offset+= 2;
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function() {
                    console.log( "complete");
                });
        });
    });
    </script>


    <?php get_footer();
