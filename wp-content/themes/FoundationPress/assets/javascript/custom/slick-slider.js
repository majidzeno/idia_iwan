$(function() {
	$(".partners__slider").slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3000,
		// speed: 9000,
		cssEase: "linear",
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
					// infinite: true,
					// dots: true
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$(".timeline__tab>a").click(function() {
		$(".timeline__tab>a").removeClass("colored");
		$(this).addClass("colored");
		var $parents = $(this).parents(".timeline__tabs");
		var $target = $parents.find("li.is-active").attr("id");
		if ($target === "tab_1") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__1").addClass("clone__tab--active");
			console.log("1");
		}
		if ($target === "tab_2") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__2").addClass("clone__tab--active");
			console.log("2");
		}
		if ($target === "tab_3") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__3").addClass("clone__tab--active");
			console.log("3");
		}
		if ($target === "tab_4") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__4").addClass("clone__tab--active");
			console.log("4");
		}
		if ($target === "tab_5") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__5").addClass("clone__tab--active");
			console.log("5");
		}
		if ($target === "tab_6") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__6").addClass("clone__tab--active");
			console.log("6");
		}
		if ($target === "tab_7") {
			$(".clone__tabs")
				.find(".clone__tab--active")
				.removeClass("clone__tab--active");
			$("#clone__7").addClass("clone__tab--active");
			console.log("7");
		}
		// console.log($target);
	});
	$(".clone__tab").click(function(e) {
		// e.preventDefault();
		console.log(true);
		$(".timeline__tabs").find(".is-active").removeClass(".is-active");
		var $tab = $(this).attr("id");
		console.log($tab);
		if ($tab == "clone__1") {
			$("#tab_1").addClass("is-active");
		}
		if ($tab == "clone__2") {
			$("#tab_2").addClass("is-active");
		}
		if ($tab == "clone__3") {
			$("#tab_3").addClass("is-active");
		}
		if ($tab == "clone__4") {
			$("#tab_4").addClass("is-active");
		}
		if ($tab == "clone__5") {
			$("#tab_5").addClass("is-active");
		}
		if ($tab == "clone__6") {
			$("#tab_6").addClass("is-active");
		}
		if ($tab == "clone__7") {
			$("#tab_7").addClass("is-active");
		}
	});
	// Page Who We Are
	$(".team__members").slick({
		slidesToShow: 4,
		slidesToScroll: 1,
	  infinite: true,
		prevArrow:
			"<button type='button' class='slick-prev members member__prev hidden'><span class='member__prev'>&#10229;</span></button>",
		nextArrow:
			"<button type='button' class='slick-next members member__next'><spanclass='member__next'>&#10230;</span></button>"
	});
	$(".team__members--mobile").slick({
		prevArrow:
			"<button type='button' class='slick-prev members member__prev hidden'><span class='member__prev'>&#10229;</span></button>",
		nextArrow:
			"<button type='button' class='slick-next members member__next'><spanclass='member__next'>&#10230;</span></button>"
	});
	$(".member__image").click(function() {
		$(".team__members")
			.find(".member__info--open")
			.removeClass("member__info--open");
		$(".team__members")
			.find(".member__tag--open")
			.removeClass("member__tag--open");
		$(".team__members")
			.find(".member__image")
			.removeClass("member__image--color");
		$(".team__members").find(".member").css("left", "0");
		var parent = $(this).parents(".member");
		parent.find(".member__image").addClass("member__image--color");
		parent.find(".member__tag").addClass("member__tag--open");
		parent.find(".member__info").addClass("member__info--open");
		var $shift = $(".member").css("width");
		parent.nextAll().css("left", $shift);
	});

$('.team__members').on('afterChange', function(event, slick, currentSlide, nextSlide){
	if (currentSlide === 0) {
		$('.slick-prev').removeClass('hidden');
		$('.slick-prev').addClass('hidden');
		// console.log('currentslide is 0');
	}
	else{
		$('.slick-prev').removeClass('hidden');
		// console.log('current slide is NOT 0');
	}
});
$('.team__members--mobile').on('afterChange', function(event, slick, currentSlide, nextSlide){
	if (currentSlide === 0) {
		$('.slick-prev').removeClass('hidden');
		$('.slick-prev').addClass('hidden');
		// console.log('currentslide is 0');
	}
	else{
		$('.slick-prev').removeClass('hidden');
		// console.log('current slide is NOT 0');
	}
});
	$(".slick-prev.members").click(function() {
		$(".team__members")
			.find(".member__info--open")
			.removeClass("member__info--open");
		$(".team__members").find(".member").css("left", "0");
		$(".team__members")
			.find(".member__tag--open")
			.removeClass("member__tag--open");
		$(".team__members")
			.find(".member__image")
			.removeClass("member__image--color");
	});
	$(".slick-next.members").click(function() {			
		$(".team__members")
			.find(".member__info--open")
			.removeClass("member__info--open");
		$(".team__members").find(".member").css("left", "0");
		$(".team__members")
			.find(".member__tag--open")
			.removeClass("member__tag--open");
		$(".team__members")
			.find(".member__image")
			.removeClass("member__image--color");
	});
	$(".architecture").click(function() {
		$(".service").removeClass("service--open");
		$(".branding").removeClass("branding--open");
		$(".strategic").removeClass("strategic--open");
		$(".projectMan").removeClass("projectMan--open");
		$(".landscape").addClass("landscape--open");
		$(".urban").addClass("urban--open");
		$(".product").addClass("product--open");
		$(".architecture").addClass("architecture--open");
	});

	// $(".urban").click(function() {
	// 	$(".service").removeClass("service--open");
	// 	$(".architecture").removeClass("architecture--open");
	// 	$(".branding").removeClass("branding--open");
	// 	$(".landscape").removeClass("landscape--open");
	// 	$(".strategic").removeClass("strategic--open");
	// 	$(".projectMan").removeClass("projectMan--open");
	// 	$(".product").removeClass("product--open");
	// 	$(".urban").addClass("urban--open");
	// });
	// $(".product").click(function() {
	// 	$(".service").removeClass("service--open");
	// 	$(".architecture").removeClass("architecture--open");
	// 	$(".branding").removeClass("branding--open");
	// 	$(".landscape").removeClass("landscape--open");
	// 	$(".strategic").removeClass("strategic--open");
	// 	$(".projectMan").removeClass("projectMan--open");
	// 	$(".urban").removeClass("urban--open");
	// 	$(".product").addClass("product--open");
	// });
	// $(".landscape").click(function() {
	// 	$(".service").removeClass("service--open");
	// 	$(".architecture").removeClass("architecture--open");
	// 	$(".branding").removeClass("branding--open");
	// 	$(".urban").removeClass("urban--open");
	// 	$(".strategic").removeClass("strategic--open");
	// 	$(".projectMan").removeClass("projectMan--open");
	// 	$(".product").removeClass("product--open");
	// 	$(".landscape").addClass("landscape--open");
	// });

	$(".projectMan").click(function() {
		$(".service").removeClass("service--open");
		$(".architecture").removeClass("architecture--open");
		$(".branding").removeClass("branding--open");
		$(".landscape").removeClass("landscape--open");
		$(".urban").removeClass("urban--open");
		$(".strategic").removeClass("strategic--open");
		$(".product").removeClass("product--open");
		$(".projectMan").addClass("projectMan--open");
	});

	$(".strategic").click(function() {
		$(".architecture").removeClass("architecture--open");
		$(".landscape").removeClass("landscape--open");
		$(".projectMan").removeClass("projectMan--open");
		$(".urban").removeClass("urban--open");
		$(".product").removeClass("product--open");
		$(".branding").addClass("branding--open");
		$(".service").addClass("service--open");
		$(".strategic").addClass("strategic--open");
	});

	// $(".service").click(function() {
	// 	$(".urban").removeClass("urban--open");
	// 	$(".architecture").removeClass("architecture--open");
	// 	$(".branding").removeClass("branding--open");
	// 	$(".landscape").removeClass("landscape--open");
	// 	$(".strategic").removeClass("strategic--open");
	// 	$(".projectMan").removeClass("projectMan--open");
	// 	$(".product").removeClass("product--open");
	// 	$(".service").addClass("service--open");
	// });

	// $(".branding").click(function() {
	// 	$(".service").removeClass("service--open");
	// 	$(".architecture").removeClass("architecture--open");
	// 	$(".urban").removeClass("urban--open");
	// 	$(".landscape").removeClass("landscape--open");
	// 	$(".strategic").removeClass("strategic--open");
	// 	$(".projectMan").removeClass("projectMan--open");
	// 	$(".product").removeClass("product--open");
	// 	$(".branding").addClass("branding--open");
	// });
});
