<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );
show_admin_bar( false );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );

// Theme Setup
// function idiaTheme_setup(){
// 	// Add Post Format
// 	add_theme_support('post-formats',array('aside','gallery','link'));
// }	


// Cutom Post Type 
add_action( 'init', 'create_post_type' );
function create_post_type() {
register_post_type( 'article',
	    array(
	      'labels' => array(
	        'name' => __( 'Articles' ),
	        'singular_name' => __( 'Articles' )
	      ),
	      'public' => true,
	      'has_archive' => true,
	      'supports' => array('thumbnail','editor','title','excerpt','custom-fields')
	    )
	  );
register_post_type( 'news',
	    array(
	      'labels' => array(
	        'name' => __( 'News' ),
	        'singular_name' => __( 'News' )
	      ),
	      'public' => true,
	      'has_archive' => true,
	      'supports' => array('thumbnail','editor','title','excerpt','custom-fields')
	    )
	  );	
register_post_type('project',
	array(
		'labels'=> array(
			'name'=>__('Projects'),
			'singular_name' => __('Projects')
		),
		'public' => true,
		'has_archive' => true,
		'supports' => array('thumbnail','editor','title','excerpt','custom-fields'),
		'taxonomies' => array('cateogory','projects'),
		'rewrite' => array('slug' => 'project')
	)
);
}


// hook into the init action and call create_project_taxonomies when it fires
add_action( 'init', 'create_projectPeriods_taxonomies', 0 );

// create two taxonomies, Projects and projects for the post type "project"
function create_projectPeriods_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Projects Periods', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Project Period', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Projects Periods', 'textdomain' ),
		'all_items'         => __( 'All Projects Periods', 'textdomain' ),
		'parent_item'       => __( 'Parent Project Period', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Project Period:', 'textdomain' ),
		'edit_item'         => __( 'Edit Project Period', 'textdomain' ),
		'update_item'       => __( 'Update Project Period', 'textdomain' ),
		'add_new_item'      => __( 'Add New Project Period', 'textdomain' ),
		'new_item_name'     => __( 'New Project Period Name', 'textdomain' ),
		'menu_name'         => __( 'Project Period', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'project_period' ),
	);
	register_taxonomy( 'project_period', 'project', $args );
}


function new_excerpt_more($more) {
       global $post;
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');



/* New excerpt length of 120 words*/
function custom_length_excerpt($word_count_limit) {
    $content = wp_strip_all_tags(get_the_excerpt() , true );
    echo wp_trim_words($content, $word_count_limit);
}


function getMorePosts(){
	$offset = $_GET['offset'];
	$exclude = $_GET['exclude'];
	$args = array(
		'post_type'=>'article',
		'posts_per_page'=>2,
		'offset'=>$offset,
	    'post__not_in'=>array($exclude)
		);
	$query= new WP_Query($args);
	$arr = [];
	$arr['posts'] = [];
	while( $query->have_posts() ) {
       $query->the_post();
       $post = [];
       $post['title'] = get_the_title();
       $post['content'] = get_the_content();
       $post['thumbnail'] = get_the_post_thumbnail(get_the_id(),'large' );
       $post['excerpt'] = get_the_excerpt();
       $post['link'] = get_the_permalink();
       array_push($arr['posts'], $post);
    }

    $args2 = array(
		'post_type'=>'article',
		'posts_per_page'=>2,
		'offset'=>$offset+2,
	    'post__not_in'=>array($exclude)
		);
	$query2= new WP_Query($args2);

    $arr['next_posts'] = $query2->post_count;
    
	echo json_encode($arr);
	wp_die();
}
	add_action( 'wp_ajax_get_posts', 'getMorePosts');
	add_action( 'wp_ajax_nopriv_get_posts', 'getMorePosts');



function getMoreNews(){
	$offset = $_GET['offset'];
	$exclude = $_GET['exclude'];
	$args = array(
		'post_type'=>'news',
		'posts_per_page'=>2,
		'offset'=>$offset,
	    'post__not_in'=>array($exclude)
		);
	$query= new WP_Query($args);
	$arr = [];
	$arr['posts'] = [];
	while( $query->have_posts() ) {
       $query->the_post();
       $post = [];
       $post['title'] = get_the_title();
       $post['content'] = get_the_content();
       $post['thumbnail'] = get_the_post_thumbnail(get_the_id(),'large' );
       $post['excerpt'] = get_the_excerpt();
       // $post['custom_excerpt']=custom_length_excerpt(20);
       $post['link'] = get_the_permalink();
       array_push($arr['posts'], $post);
    }

$args2 = array(
	'post_type'=>'news',
	'posts_per_page'=>2,
	'offset'=>$offset+2,
	'post__not_in'=>array(exclude)
	);
$query2=new WP_Query($args2);
$arr['next_posts']=$query2->post_count;

	echo json_encode($arr);
	wp_die();
}
	add_action( 'wp_ajax_get_news', 'getMoreNews');
	add_action( 'wp_ajax_nopriv_get_news', 'getMoreNews');


function getMoreProjects(){
	$offset = $_GET['offset'];
	$exclude = $_GET['exclude'];
	$args = array(
		'post_type'=>'project',
		'posts_per_page'=>2,
		'offset'=>$offset,
	    'post__not_in'=>array($exclude)
		);
	$query= new WP_Query($args);
	$arr = [];
	$arr['posts'] = [];
	while( $query->have_posts() ) {
       $query->the_post();
       $post = [];
       $post['title'] = get_the_title();
       $post['content'] = get_the_content();
       $post['thumbnail'] = get_the_post_thumbnail(get_the_id(),'large' );
       $post['excerpt'] = get_the_excerpt();
       // $post['custom_excerpt']=custom_length_excerpt(20);
       $post['link'] = get_the_permalink();
       array_push($arr['posts'], $post);
    }

$args2 = array(
	'post_type'=>'project',
	'posts_per_page'=>2,
	'offset'=>$offset+2,
	'post__not_in'=>array(exclude)
	);
$query2=new WP_Query($args2);
$arr['next_posts']=$query2->post_count;

	echo json_encode($arr);
	wp_die();
}
	add_action( 'wp_ajax_get_projects', 'getMoreProjects');
	add_action( 'wp_ajax_nopriv_get_projects', 'getMoreProjects');
