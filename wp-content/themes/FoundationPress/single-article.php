<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" role="main">

<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		<header>
		  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
		  	<div class="bg_image" style="background-image: url('<?php echo $image[0] ?>');">
		  		<div class="dimmed" style="background-image: url('<?php echo get_stylesheet_directory_uri()?>/assets/images/Post/dimmed.png');"></div>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</div>
		</header>
		<div class="entry-content">
			<?php the_content(); ?>
        <?php if( have_rows('sub_article') ): ?>
            <div class="row subArticle collapse" id="single__subArticle">
                <?php while ( have_rows('sub_article') ) : the_row(); ?>
                <div class="large-2 columns subArticle__icons">
                    <ul class="tabs vertical" id="vert-tabs" data-tabs  data-deep-link="false" data-deep-link-smudge="false" >
                        <li class="tabs-title is-active triangle">
                            <a href="#panel1" aria-selected="true">               
                                <?php the_sub_field('top_icon') ;?>
                            </a>
                        </li>
                        <li class="tabs-title">
                            <a href="#panel2">
                                <?php the_sub_field('middle_icon') ;?>
                            </a>
                        </li>
                        <li class="tabs-title">
                            <a href="#panel3">
                                <?php the_sub_field('bottom_icon') ;?>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="large-10 columns subArticle__text">
                    <div class="tabs-content vertical" data-tabs-content="vert-tabs">
                        <div class="tabs-panel is-active" id="panel1">
                            <?php the_sub_field('sub_text1') ?>      
                        </div>
                        <div class="tabs-panel" id="panel2">
                            <?php the_sub_field('sub_text2') ?>
                        </div>
                        <div class="tabs-panel" id="panel3">
                            <?php the_sub_field('sub_text3') ?>
                        </div>
                    </div>
                </div>
                <?php endwhile;
                    else :
            endif;?>
            </div>
        </div>
	</article>
<?php endwhile;?>
</div>
<?php
    $args = array(
      'post_type' => 'article',
      'posts_per_page'=>2,
      'post__not_in'=>array($post->ID)
    );
    $articles = new WP_Query( $args );
    $posts = $articles->found_posts;
    if( $articles->have_posts() ) {?>
        <div class="row postsContainer" data-equalizer>
        	<h2 class="postsContainer__title">
        		More Articles
        	</h2>
    	<?php  while( $articles->have_posts() ) {
        	$articles->the_post();
        	?>
            <div class="large-6 small-12 columns morePost" data-mh="post">
                <div class="" >
                    <div class="large-5 small-12 columns morePost__image" data-mh="post_content">
                        <?php  the_post_thumbnail( 'large' );?>
                    </div>
                    <div class="large-7 small-12 columns morePost__text" data-mh="post_content">
                        <a href="<?php the_permalink(); ?>"><h5 class="morePost__title"><?php the_title() ?></h5></a>
                        <div class="content morePost__excerpt">
                            <?php the_excerpt(); ?>
                            <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
      }
    } ?>
	    </div>
    <?php if($posts > 2) { ?>
	<div class="verge">
		<a href="#" class="button button__blue" data-load-posts="2" data-exclude = "<?php echo $post->ID ?>">
			Load More Articles 
		</a>	
	</div>
    <?php } ?>


<script type="text/javascript">
    $(document).ready(function(){
        
       $('li.tabs-title').click(
            function(){
                $('.tabs-title').removeClass('triangle');
                // $('.main').removeClass('hidden');
                $(this).addClass('triangle');
                // $(this).find('.main').addClass('hidden');
            }
        );
        var $offset = parseInt($("[data-load-posts]").attr('data-load-posts'));
        var exclude = $("[data-load-posts]").attr('data-exclude');
        $("[data-load-posts]").click(function(e){
            e.preventDefault();
            $.ajax({
                url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type:"GET",
                data:{action:'get_posts',offset:$offset,exclude:exclude}
                })
                .done(function(response){
                    var $response = JSON.parse(response);
                    console.log(response)
                    for (var i = 0; i <= $response.posts.length-1; i++){
                             $(".postsContainer").append(`
                          <div class='large-6 columns morePost' data-mh="post">
                            <div >
                              <div class='large-5 small-12 columns morePost__image' data-mh="post_content">${$response.posts[i]['thumbnail']}</div>
                              <div class='large-7 small-12 columns morePost__text' data-mh="post_content">
                                <a href="${$response.posts[i]['link']}">
                                  <h5 class='morePost__title'>${$response.posts[i]['title']}</h5>
                                </a>
                                <div class='content morePost__excerpt'>${$response.posts[i]['excerpt']}</div>
                                <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                              </div>
                            </div>
                          </div>`);
                    };
                    jQuery.fn.matchHeight._apply('[data-mh]');
                    if($response.next_posts == 0) {
                         $(".verge").remove();
                    }

                    $offset+= 2;
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function() {
                    console.log( "complete");
                });
        });
    });
    </script>


    <?php get_footer();
