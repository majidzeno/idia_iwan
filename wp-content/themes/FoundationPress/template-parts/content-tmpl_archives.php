<?php
/**
* The template used to display archive content
*/
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(''); ?>>
	<header>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?/**php foundationpress_entry_meta(); */?>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
		
	</div>
</div>
