<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
get_header(); ?>
<?php 
  $period = get_the_terms(get_the_ID(), 'project_period'); 
    // var_dump();
 ?>
<div id="single-post" role="main">
    <div class="clone show-for-large">
        <ul class="timeline__tabs clone__tabs">
            <li class="timeline__tab clone__tab" id="clone__1"><a href="<?php echo get_page_link(115); ?>" >News</a></li>
            <li class="timeline__tab clone__tab <?php echo ($period[0]->name == '2016 - 2020') ? 'clone__tab--active' : '' ?>" id="clone__6"><a href="<?php echo get_page_link(115); ?>?tab=tab_6">2016 - 2020</a></li>
            <li class="timeline__tab clone__tab <?php echo ($period[0]->name == '2011 - 2015') ? 'clone__tab--active' : '' ?>" id="clone__5"><a href="<?php echo get_page_link(115); ?>?tab=tab_5">2011 - 2015</a></li>
            <li class="timeline__tab clone__tab <?php echo ($period[0]->name == '2006 - 2010') ? 'clone__tab--active' : '' ?>" id="clone__4"><a href="<?php echo get_page_link(115); ?>?tab=tab_4">2006 - 2010</a></li>
            <li class="timeline__tab clone__tab <?php echo ($period[0]->name == '2001 - 2005') ? 'clone__tab--active' : '' ?>" id="clone__3"><a href="<?php echo get_page_link(115); ?>?tab=tab_3">2001 - 2005</a></li>
            <li class="timeline__tab clone__tab <?php echo ($period[0]->name == '1995 - 2000') ? 'clone__tab--active' : '' ?>" id="clone__2"><a href="<?php echo get_page_link(115); ?>?tab=tab_2">1995 - 2000</a></li>
            <li class="timeline__tab clone__tab" id="clone__7"><a href="<?php echo get_page_link(115); ?>?tab=tab_7">All Projects</a></li>
        </ul>
    </div>
<?php while ( have_posts() ) : the_post(); ?>
    <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
    
    <?php if( have_rows('project_slider') ): ?>
    <div class="projectSlider">
      <?php // loop through the rows of data
        while ( have_rows('project_slider') ) : the_row();?>
                <?php $values =get_sub_field('project_link');
                    if( $values ){
                        $video="video";
                    }else{
                        $video="";
                    } ?>                
            <div><div class="projectSlider__image <?php echo $video?>" style="background-image: url(<?php echo get_sub_field('project_slide');?> )">
                <?php echo ( get_sub_field('project_link') );   ?>
            </div></div>

        <?php endwhile; ?>
    </div>
    <?php else :
        endif;?>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
    </article>
<?php endwhile;?>
</div>
<!--Custom MorePosts  -->
<?php
    $args = array(
      'post_type' => 'project',
      'posts_per_page'=>2,
      'post__not_in'=>array($post->ID)
    );
    $projects = new WP_Query( $args );
    $posts = $projects->found_posts;
    if( $projects->have_posts() ) {?>
        <div class="row postsContainer" >
            <h2 class="postsContainer__title">
                More Projects
            </h2>
        <?php  while( $projects->have_posts() ) {
            $projects->the_post();
            ?>
            <div class="large-6 small-12 columns morePost" data-mh="post">
                <div class="" >
                    <div class="large-5 small-12 columns morePost__image" data-mh="post_content">
                        <?php  the_post_thumbnail( 'large' );?>
                    </div>
                    <div class="large-7 small-12 columns morePost__text" data-mh="post_content">
                        <a href="<?php the_permalink(); ?>"><h5 class="morePost__title"><?php the_title() ?></h5></a>
                        <div class="content morePost__excerpt">
                            <?php the_excerpt(); ?>
                            <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
      }
    } ?>
        </div>
        <!-- Load More Button  -->
    <?php if($posts > 2) { ?>
    <div class="verge">
        <a href="#" class="button button__blue" data-load-posts="2" data-exclude = "<?php echo $post->ID ?>">
            Load More Projects 
        </a>    
    </div>
    <?php } ?>


<script type="text/javascript">
    $(document).ready(function(){
        var $offset = parseInt($("[data-load-posts]").attr('data-load-posts'));
        var exclude = $("[data-load-posts]").attr('data-exclude');
        $("[data-load-posts]").click(function(e){
            e.preventDefault();
            $.ajax({
                url:"<?php echo admin_url( 'admin-ajax.php' ); ?>",
                type:"GET",
                data:{action:'get_projects',offset:$offset,exclude:exclude}
                })
                .done(function(response){
                    var $response = JSON.parse(response);
                    // if ($response.length == 0 ) {
                    //     $(".verge").remove();
                    // }
                    for (var i = 0; i <= $response.posts.length-1; i++){
                             $(".postsContainer").append(`
                          <div class='large-6 columns morePost' data-mh="post">
                            <div >
                              <div class='large-5 small-12 columns morePost__image' data-mh="post_content">${$response.posts[i]['thumbnail']}</div>
                              <div class='large-7 small-12 columns morePost__text' data-mh="post_content">
                                <a href="${$response.posts[i]['link']}">
                                  <h5 class='morePost__title'>${$response.posts[i]['title']}</h5>
                                </a>
                                <div class='content morePost__excerpt'>${$response.posts[i]['excerpt']}</div>
                                <a class="moretag" href="<?php echo get_permalink($post->ID) ?>"><span> Read More </span></a>
                              </div>
                            </div>
                          </div>`);
                    };

                        jQuery.fn.matchHeight._apply('[data-mh]');



                    if($response.next_posts == 0) {
                         $(".verge").remove();
                    }

                    $offset+= 2;
                })
                .fail(function(){
                    console.log("error");
                })
                .always(function() {
                    console.log( "complete");
                });
        });
    });
    </script>


    <?php get_footer();
